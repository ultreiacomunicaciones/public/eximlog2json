Script to parse exim logfiles and output them as json

Please note that this script expects a mix of exim mainlog, rejectlog and paniclog. 
Also, the resulting file is expected to have syslog tags from a  remote syslog server. 

This code is released under GPLv3.