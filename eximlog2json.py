import argparse
import json
import re

if __name__ == '__main__':
    argp = argparse.ArgumentParser()
    argp.add_argument('-i', '--input', type=str, required=True)
    arguments = argp.parse_args()
    lc = 1
    with open(arguments.input, "r") as file:
        for line in file:
            lc += 1
            parsedline = {}

            # For each line we will parse, make a decission on which type it is and generate a json
            lineparts = line.split(" ")
            parsedline['syslog_timestamp'] = " ".join(lineparts[0:3])
            parsedline['reporting_hostname'] = lineparts[3]
            parsedline['process_and_pid'] = lineparts[4].replace(':', '')
            parsedline['wholemessge'] = " ".join(lineparts[5:])
            if lineparts[5][0] == '[':  # Lines starting with "[1/53] blabla" are just debug: ignored.
                parsedline['debug_info'] = " ".join(lineparts[5:])
            else:
                parsedline['exim_timestamp'] = " ".join(lineparts[5:7])
                # Is the next token an exim ID?
                if re.match(r'\w{6}-\w{6}-\w{2}', lineparts[7], re.I):
                    parsedline['exim_id'] = lineparts[7]
                    m = re.match(r'(<=|=>|->|\*\*|==|Completed)', lineparts[8])
                    if m:  # Look for a flag
                        i = 8
                        # Flag found
                        flag = m.group(1)
                        parsedline['flag'] = flag
                        if flag == '<=':
                            parsedline['exim_event'] = 'message_arrival'
                            parsedline['sender'] = lineparts[i + 1]
                            flag_R_name = "exim_bounce_reference"
                            i += 1
                        elif flag == '(=':
                            parsedline['exim_event'] = 'message_fakereject'
                        elif flag == '=>':
                            parsedline['exim_event'] = 'normal_message_delivery'
                            parsedline['recipient'] = lineparts[i + 1]
                            flag_R_name = "exim_router"
                            i += 1
                        elif flag == '->':
                            parsedline['exim_event'] = 'additional_message_delivery'
                            parsedline['recipient'] = lineparts[i + 1]
                            flag_R_name = "exim_router"
                            i += 1
                        elif flag == '>>':
                            parsedline['exim_event'] = 'cutthrough_message_delivery'
                            flag_R_name = "exim_router"
                        elif flag == '*>':
                            parsedline['exim_event'] = 'delivery_supressed_by_N'
                        elif flag == '**':
                            parsedline['exim_event'] = 'delivery_failed_address_bounced'
                            flag_R_name = "exim_router"
                        elif flag == '==':
                            parsedline['exim_event'] = 'delivery_deferred_temporary_problem'
                            flag_R_name = "exim_router"
                        elif flag == 'Completed':
                            parsedline['exim_event'] = 'message_processing_complete'
                        else:
                            parsedline['exim_event'] = 'PARSE_ERROR_unrecognized_event'
                        # Now parse the rest of the line
                        while i < len(lineparts):
                            m = re.match(r'(\w+)=(.+)', lineparts[i])
                            if m:
                                tag = m.group(1)
                                if tag == "R":
                                    parsedline[flag_R_name] = m.group(2)
                                elif tag == "T":
                                    parsedline['exim_transport'] = m.group(2)
                                elif tag == "id":
                                    parsedline['email_message_id'] = m.group(2)
                                elif tag == "H":
                                    parsedline['peer_hostname'] = m.group(2)
                                    m = re.match(r'\[(.+)\]', lineparts[i + 1])
                                    if m:
                                        parsedline['peer_ipaddress'] = m.group(1)
                                        i += 1
                            i += 1
                    else:  # flag not found
                        parsedline['log_info'] = " ".join(lineparts[8:])
                else:
                    # we try to extract hostname and from...
                    i = 7
                    while i < len(lineparts) and \
                            ('peer_hostname' not in parsedline.keys() or 'sender' not in parsedline.keys()):
                        # Get peer host and rest of the line, if it starts with H=....
                        m = re.match(r'H=(.*)', lineparts[i])
                        if m:
                            parsedline['peer_hostname'] = m.group(1)
                            m = re.match(r'\[(.+)\]', lineparts[i + 1])
                            if m:
                                parsedline['peer_ipaddress'] = m.group(1)
                                i += 1
                        m = re.match(r'F=<(.*)>', lineparts[i])
                        if m:
                            parsedline['sender'] = m.group(1)
                        i += 1
                    parsedline['log_info'] = " ".join(lineparts[i:])

            # next toket defines wether this line is a flagged line or not
            print(json.dumps(parsedline))
            print("")
            # Is it a flagged line or not

            pass
